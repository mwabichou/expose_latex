# La décomposition en produit de facteurs premiers

Exposé en LaTeX sur la décomposition en produit de facteurs premiers (En mathématiques et plus précisément en arithmétique, la décomposition en produit de facteurs premiers, aussi connue comme la factorisation entière en nombres premiers), toute en partageant les tâches du travail sur Git; voici la table des matières :

![Screenshot](toc.png)

## Pour commencer

Placer vous dans le dossier du projet avec la commande cd

### Pré-requis

Ce qu'il est requis pour commencer avec votre projet...

- Un logiciel destiné à l'édition de documents LaTeX, exemple : Texmaker
- Un logiciel pour manipuler des documents électroniques au format PDF, exemple : Adobe Acrobat

### Installation

1 - Lancer un terminal dans le dossier du projet
Executez la commande `latex Projet.tex` pour compiler de transformer vos fichiers sources (.tex, .bib) contenant les instructions de mise en page et le contenu en un document final (généralement au format .pdf)

## Fabriqué avec

- [LaTeX](https://www.latex-project.org/help/documentation/) - Langage de composition de documents

## Versions

**Dernière version stable :** 1.0
Liste des versions : [Cliquer pour afficher](https://gitlab.isima.fr/mwabichou/expose_latex/-/tags)

## Auteurs

- **Mohamed Wassim Abichou** _alias_ [@mwabichou](https://gitlab.isima.fr/mwabichou)
- **M'hamed Farah Dissou** _alias_ [@mfdissou](https://gitlab.isima.fr/mfdissou)

Lisez la liste des [contributeurs](https://gitlab.isima.fr/mwabichou/expose_latex/-/project_members) pour voir qui à aidé au projet !

## License

Ce projet est sous la licence `UCA`
